package com.czxbnb.sdkdemo;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.chur.indoorlocation.sdk.ChurSDK;


public class DemoApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ChurSDK.getInstance(this, "development", "development");
        ChurSDK.setDebugEnabled(true);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}
