package com.chur.indoorlocation.data

import com.churinc.tymonlibrary.RxHttpUtils
import com.churinc.tymonlibrary.interceptor.Transformer
import com.churinc.tymonlibrary.observer.DataObserver
import com.google.gson.JsonObject
import com.chur.indoorlocation.sdk.jni.JNIManager
import com.chur.indoorlocation.sdk.model.ApiService
import com.chur.indoorlocation.sdk.model.user.LoggedInUser

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    interface OnLoginCompleted{
        fun onSuccess(user: LoggedInUser)
        fun onFailure(message: String)
    }

    fun login(username: String, password: String, listener: OnLoginCompleted){
        val jsonObject = JsonObject()

        jsonObject.addProperty("username", username)
        jsonObject.addProperty("password", password)
        jsonObject.addProperty("business_id", "8f15967c-d321-4a72-9580-da0db944d474")

        RxHttpUtils.getSInstance()
                .baseUrl(JNIManager.getURL())
                .createSApi(ApiService::class.java)
                .login(jsonObject)
                .compose(Transformer.switchSchedulers())
                .subscribe(object : DataObserver<LoggedInUser>() {
                    override fun onError(errorMsg: String) {
                        listener.onFailure(errorMsg)
                    }

                    override fun onSuccess(data: LoggedInUser) {
                        listener.onSuccess(data)
                    }
                })

    }

    fun logout() {
        // TODO: revoke authentication
    }
}

