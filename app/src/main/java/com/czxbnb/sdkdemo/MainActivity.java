package com.czxbnb.sdkdemo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.chur.indoorlocation.sdk.manager.ChurLocationManager;
import com.chur.indoorlocation.sdk.manager.PermissionManager;
import com.chur.indoorlocation.sdk.service.keepalive.ServiceAliveUtils;
import com.chur.indoorlocation.sdk.utils.permission.RequestCallback;

import java.util.List;

import static android.Manifest.permission.ACCESS_BACKGROUND_LOCATION;
import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    ChurLocationManager churLocationManager = ChurLocationManager.Companion.getInstance(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getColor(R.color.colorAccent));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            PermissionManager.with(this).permissions(
                    ACCESS_FINE_LOCATION,
                    ACCESS_COARSE_LOCATION,
                    WRITE_EXTERNAL_STORAGE,
                    ACCESS_BACKGROUND_LOCATION
            ).request(new RequestCallback() {
                @Override
                public void onDenied(List<String> deniedAndNextAskList, List<String> deniedAndNeverAskList) {
                    super.onDenied(deniedAndNextAskList, deniedAndNeverAskList);
                }

                @Override
                public void onAllGranted() {

                }
            });
        } else {
            PermissionManager.with(this).permissions(
                    ACCESS_FINE_LOCATION,
                    ACCESS_COARSE_LOCATION,
                    WRITE_EXTERNAL_STORAGE
            ).request(new RequestCallback() {
                @Override
                public void onDenied(List<String> deniedAndNextAskList, List<String> deniedAndNeverAskList) {
                    super.onDenied(deniedAndNextAskList, deniedAndNeverAskList);
                }

                @Override
                public void onAllGranted() {

                }
            });
        }


        TextView tvSettings = findViewById(R  .id.tv_settings);
        tvSettings.setOnClickListener(v -> {
            showSettings();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Button btnStart = findViewById(R.id.btn_start);
        boolean serviceStart = ServiceAliveUtils.isBeaconServiceAlive();
        if (serviceStart) {
            btnStart.setText("Stop Service");
        } else {
            btnStart.setText("Start Service");
        }

        btnStart.setOnClickListener(v -> {
            boolean isServiceStart = ServiceAliveUtils.isBeaconServiceAlive();
            if (!isServiceStart) {
                churLocationManager.startService("Demo App", "App is running at background");
                btnStart.setText("Stop Service");
            } else {
                churLocationManager.stopService();
                btnStart.setText("Start Service");
            }
        });
    }

    private void showSettings() {
        try {
            Intent intent;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                intent = new Intent(Settings.ACTION_BATTERY_SAVER_SETTINGS);
            } else {
                intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            }
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        } catch (Exception e) {
            //Open the specific App Info page:
            Intent intent = new Intent(Settings.ACTION_APPLICATION_SETTINGS);
            startActivity(intent);
        }
    }
}
