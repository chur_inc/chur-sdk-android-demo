package com.czxbnb.sdkdemo

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import com.chur.indoorlocation.data.LoginDataSource
import com.chur.indoorlocation.data.LoginRepository
import com.chur.indoorlocation.sdk.ChurSDK
import com.chur.indoorlocation.sdk.model.user.LoggedInUser
import com.chur.indoorlocation.sdk.model.user.User
import com.chur.indoorlocation.sdk.preference.PreferenceUtils
import com.churinc.tymonlibrary.RxHttpUtils
import com.churinc.tymonlibrary.utils.ToastUtils
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import java.util.*


class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.statusBarColor = getColor(R.color.colorAccent)
        }

        if ( PreferenceUtils.getInstance().username != null) {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }

        btn_login.setOnClickListener {
            RxHttpUtils.init(this)
            login(et_username.text.toString(), et_password.text.toString())
            btn_login.text = getString(R.string.logging_in)
            btn_login.isEnabled = false
        }
    }

    private fun login(username: String, password: String) {
        // can be launched in a separate asynchronous job
        val loginRepository = LoginRepository(
            dataSource = LoginDataSource()
        )
        loginRepository.login(username.trim().toLowerCase(Locale.getDefault()),
            password, object : LoginDataSource.OnLoginCompleted {
            override fun onSuccess(user: LoggedInUser) {
                val user = User()

                user.businessId = PreferenceUtils.getInstance().businessId
                user.businessUserId = PreferenceUtils.getInstance().businessUserId
                user.username = PreferenceUtils.getInstance().username

                ChurSDK.setUser(user)
                val intent = Intent(applicationContext, MainActivity::class.java)
                startActivity(intent)

            }

            override fun onFailure(message: String) {
                ToastUtils.showLongToast(message)
                btn_login.text = getString(R.string.login)
                btn_login.isEnabled = true
            }
        })
    }


}